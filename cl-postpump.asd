
(asdf:defsystem cl-postpump
  :name "cl-postpump"
  :version "0.2.0"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :license "AGPLv3+"
  :depends-on ("cl-ppcre" "drakma" "lquery" "plump")
  :components ((:module "src"
			 :components
			 ((:file "package")
			  (:file "postpump")
			  (:file "article")
			  (:file "submission")
			  (:file "comment")
			  (:file "paginator"))))
  :description "a library for interfacing with postmill servers")
