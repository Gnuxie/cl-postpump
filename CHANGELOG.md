# v0.2.0 2018.10.06

## New classes
 * submission, forum and comment pagiantors added
 * submission and coment classes added

## New methods
 * paginator-fetch
 * paginator-article
 * paginator-articles
 * paginator-find-if
 * paginator-remove-if-not
 * paginator-regex
 * paginator-feed
 * vote
 * reply
 * id
 * score
 * username
 * time-posted
 * permalink
 * article-delete
 * body
 * replies
 * parent
 * parent-submission
 * submission-link
 * submission-forum
 * submission-title
 * make-submission-paginator
 * make-submission
 * make-forum
 
## New documentation
 * reference added
 * ./examples/whatbot.lisp added
 
## Other
 * vote-comment and vote-submission accept more values of different types for choice
 * test added ./test/test.lisp

# v0.1.0-alpha
 * first alpha version
