(ql:quickload :cl-postpump)
(ql:quickload :cl-ppcre)

(setf ppump:*postmill-server* "https://raddle.me")
(defparameter *username* "cl_postpump_test")
(defparameter *password* "please do not change this")
(defparameter *cookie-jar* (make-instance 'drakma:cookie-jar))

(defparameter *new-comments* (make-instance 'ppump:comment-paginator
					    :feed "/user/cl_postpump_test/comments"))

(defparameter *last-comment-id* 63731)

(defun find-whats (paginator)
  (ppump:paginator-fetch paginator)
  (let ((new-last-comment (ppump:id (ppump:paginator-article paginator 0))))
    (let ((whats (ppump:paginator-regex "^\\S*[wW]+[hH]+[aA]+[tT]+\\S*$"
					paginator
					*last-comment-id*)))
      
      (setf *last-comment-id* new-last-comment)
      whats)))

(defun reply-to-a-what (a-comment)
  (let* ((text (ppump:body (if (ppump:parent a-comment) (ppump:parent a-comment) (ppump:parent-submission a-comment)))))
    (when text
	(ppump:reply a-comment (string-upcase text) *cookie-jar*))))

(defun reply-to-whats (whats)
  (map 'vector #'reply-to-a-what whats))

(ppump:login *username* *password* *cookie-jar*)
(reply-to-whats (find-whats *new-comments*))
