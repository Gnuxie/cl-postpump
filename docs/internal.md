### %comment-get-context%


Used by the reader `parent-submission` or `parent` to get the parent comment, replies and parent-submission
for a comment that is missing either.



### refresh-submission

This is not a user function to "refresh" a page, it is similar to comment-get-context and requests the
live version of the object from postmill to obtain any attributes marked as missing


### article-get-replies

Used by make-submission to produce the replies from a submission page.
