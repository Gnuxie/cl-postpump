# Examples
Please also see the [examples](../examples/) directory.


## WARNING! it probably isn't wise to directly copy these examples, please do not spam peoples subforums

### making a cookie jar with drakma
before you can log in or comment you will first need to make a cookie-jar to store your cookies in
`(defparameter *cookie-jar* (make-instance 'drakma:cookie-jar))`


### logging in
In preparation for logging in you might want to first create a cookie-jar using drakma and change the global variable `ppump:*postmill-server*` to a server of your choice, in this example we will use jstpst.net
```lisp
(defvar *cookie-jar* (make-instance 'drakma:cookie-jar))
(setf cl-postpump:*postmill-server* "https://www.jstpst.net")
(ppump:login "cl-postpump" "password" *cookie-jar*)
```
with a bit of luck you should now be logged in and `login` will have returned the front page of jstpst.net

## Using a paginator
In this example we are going to view the submissions on the jstpst.net front page, and then look at some comments.

### creating a paginator
The first thing we need to do is create a `submission-paginator`
`(defparameter front-page (ppump:make-submission-paginator "/"))`
we could also add sorting with the keyword argument to get new submissions if we wanted to, the valid options at the moment are:
`:hot`, `:new`, `:top`, `:controversial` and `:most_commented`
if none are specified then we will see submissions sorted by hot.

Then to fetch the articles on jstpst.net's front page we just do:
`(ppump:paginator-fetch front-page)`

Now to access the articles we can use:
`(ppump:paginator-articles front-page)`
We should get in return a vector of submission objects, which in the repl will looks like this:
```
#(#<CL-POSTPUMP:SUBMISSION {1004B61383}> #<CL-POSTPUMP:SUBMISSION {1004B64703}>
  #<CL-POSTPUMP:SUBMISSION {1004B7A6A3}>)
```
but with 25 submissions instead of 3.

Please see the [reference](./reference.md) on how to interact with submission and comment objects.

### using a paginator-filter
We can filter all the articles in a paginator by using either `paginator-regex` `paginator-find-if` or `paginator-remove-if-not`

Here we will look through all the submission titles containing "free" up until we get to articles with an id less than 2201.
`(ppump:paginator-regex "free" front-page 2200)`

It's important to note that the first submission id is 1, not 0 and also as of current paginators will not use the same socket between fetching pages. This means they are relatively slow but this will probably change in the future.

## submission and comment objects
You can do everything listed under miscellaneous using the new objects. (apart from posting a submission)

### replying to a submission or comment.
`(ppump:reply an-article "Hi I'd just like to interject for a moment" *cookie-jar*)`

### voting
`(ppump:vote :up an-article *cookie-jar*)`

### deleting
`(ppump:article-delete an-article *cookie-jar*)`

## Miscellaneous 
### posting a submission
in order to post a submission we must provide a cookie-jar, a title and a forum to post to. There are optional keyword arguments `:submission-body` and `:submission-url` you can use to post those fields. The submission-url will have to be a valid url for the webserver to accept your request.
```lisp
(ppump:post-submission *cookie-jar* "test title" "test_posts" 
                             :submission-body "test body" :submission-url "www.jstpst.net")
```
if all goes well `post-submission` will return the post page, if you get directed to the submit page it means something has gone wrong.


### posting a comment
You can post comments to posts, comments and nested comments, in this example we will just post a comment to a post.

`post-comment` requires the url of the item you want to comment on, the comment as a string and the cookie-jar
```lisp
(ppump:post-comment "https://www.jstpst.net/f/test_posts/2537/test-title"
                          "hello world!"
                          *cookie-jar*)
 ```
 and just because we can this is how you would reply to a comment
 ```lisp
 (ppump:post-comment "https://www.jstpst.net/f/test_posts/2537/comment/5455"
                           "hello again!"
                           *cookie-jar*)
```
if all goes well you should be directed by the webserver to your new comment


### voting
in this example I'm going to downvote myself. `vote-submission` takes three arguments, the vote choice, the submission url and a cookie-jar.
```lisp
(ppump:vote-submission "-1" "https://www.jstpst.net/f/test_posts/2537/test-title" *cookie-jar*)
```
`vote-comment` works exactly the same way. When you vote successfully (without javascript) postmill should return you to the front page.


### deleting 
to delete a post we can use `delete-submission` and to delete a comment we can use `delete-comment`
these functions only take two arguments, the item url and a cookie-jar. 
```lisp
(ppump:delete-post "https://www.jstpst.net/f/test_posts/2537/test-title" *cookie-jar*)
```


## a note about responses
doesn't look nice does it?
```
"<!DOCTYPE html>
<html lang=\"en\" prefix=\"og: http://ogp.me/ns#\">
  <head>
    <meta charset=\"UTF-8\" />
    <title>  test title
</title>
✂----------
    <meta name=\"generator\" content=\"postmill \">
      <link rel=\"alternate\" type=\"application/atom+xml\" href=\"/f/test_posts/hot/1.atom\" title=\"test_posts\">
          <footer class=\"site-footer\">
        <p class=\"site-footer-version\">
          Running <a href=\"https://gitlab.com/edgyemma/Postmill\">Postmill</a>. Made with your posts.
        </p>
      </footer>
            </body>
</html>
"
200
((:CONNECTION . "close") (:DATE . "Wed, 05 Sep 2018 15:45:02 GMT")
 (:SERVER . "Apache") (:CACHE-CONTROL . "max-age=0, must-revalidate, private")
 (:LINK
  . "</build/; rel=\"preload\"")
 (:TRANSFER-ENCODING . "chunked") (:CONTENT-TYPE . "text/html; charset=UTF-8")
 (:VIA . "1.1 vegur"))
#<PURI:URI https://www.jstpst.net/f/test_posts/2537/comment/5456>
#<FLEXI-STREAMS:FLEXI-IO-STREAM {10032BBAB3}>
T
"OK"
```
when you use a function to post a comment or vote, you will be redirected to another page by the webserver and drakma will return relevant information for that process in 7 return values. You can read about them [here](https://edicl.github.io/drakma/#dict-request) 
