# TODO / unorganised notes

# Current

## body
should this be a plump node or be serialized

## article replies
should the replies have the 'missing or should it always be a vector and the class
have an additional slot denoting whether the articles are 'missing

## accessors on `submission` have prefixes, they shouldn't really
hmmm, they are unique to submission though, unlike the others.

## replace #() these with (make-array :fillpointer 0) use an sensible init length

## add more tests.

## string issues
`(setf cl-postpump:*postmill-server* "https://www.jstpst.net/")` instead of
`(setf cl-postpump:*postmill-server* "https://www.jstpst.net")` causes an error due to the additional "/"


## csrf issue
csrf tokens for all post requests at current are getted before a post action is submitted
ie tokens are not saved when objects are first requested as the csrf token might change
this means functions such as vote and login take up to 3 seconds at times


## parse response of post-request

right now no methods or functions but reply need the response to 
a post request to be parsed.

## make a make-paginator method
it should fetch articles once made to save this (progn fetch then return articles) nonsense
probably make generators for other classes too


## Do we need a forum class?
a user could use a submission-paginator on a forum, but they will not be able to fetch the sidebar, subscribe etc
maybe this is something that can be held back until `v0.3.0`

Here is a grep of where the `forum` class has already been used. There are no methods for it so I'm making forum
internal until methods for subscribing etc are introduced.


```grep
../src/constructors.lisp:11:  (defun make-forum (url &key (sorting :hot))
../src/constructors.lisp:12:    (let ((forum-name (subseq url 3))
../src/constructors.lisp:14:      (make-instance 'forum :feed feed :url feed :name forum-name))))
../src/package.lisp:25:   :make-forum
../src/objects.lisp:72:   (forum
../src/objects.lisp:73:    :initarg       :forum
../src/objects.lisp:74:    :initform      (error "Must supply a submission forum")
../src/objects.lisp:75:    :accessor      submission-forum
../src/objects.lisp:77:    :documentation "The relative url to the forum this submission was posted to")
../src/objects.lisp:106:(defclass forum (submission-paginator)
../src/objects.lisp:109:    :initform      (error "Must supply a forum name")
../src/objects.lisp:110:    :accessor      forum-name
../src/objects.lisp:112:    :documentation "The name of the forum")
../src/objects.lisp:116:    :initform      (error "Must supply a forum url")
../src/objects.lisp:117:    :accessor      forum-url
../src/objects.lisp:119:    :documentation "The relative URL of the forum")))
../test/test.lisp:39:(defun test-forum-paginator-fetch ()
../test/test.lisp:40:  (let ((a-paginator (cl-postpump:make-forum "/f/Icecat")))
../test/test.lisp:146:        (run-test #'test-forum-paginator-fetch)
```


# Partial

## argument ordering
maybe move cookie-jar to the first argument for all functions requiring it?
the functions get-request and post-request are made visible by the system,
I don't know how justified this is. 
So in the case of the cookie-jar argument ordering all visible functions 
except get-request and post-request require it

### what has been done
Not entirely sure this is a relevant issue anymore with the introduction of
the objects

# Past


## Better name for article-fetch
should rename article-fetch to paginator-fetch

### solution
renamed all occurrences of article-fetch paginator-fetch
as the method actually belonged to the paginator class.


## comment-get-context
should make another method for when there is a comment that already exists with it's context (parents, replies)
then make this method internal only. (and use it in the new method when there is no provided context)

### solution
internal method %comment-get-context% added. accessors for the parent-submission attribute and
a comments parent marked as low level. New accessors parent-submission and parent added respectively 
that utilise comment-get-context. 


## use smaller comment_form 
e.g. https://www.jstpst.net/comment_form/test_posts/2537/5462
found by bea in the js
https://www.jstpst.net/comment_form/0/0/0 

### solution 
a function get-reply-token was added to get the token from /comment_form/0/0/0


## submission :forum
on front pages submission articles have a forum attribute, in forums they don't 
the article fetch clause is a horrid mess as a consequence

### solution
parse-to-submission takes the forum attribute from the url using a regular expression
when there is no forum attribute. This actually doesn't add much time if any to the 
parsing of the submission (which i was slightly worried about) since the network takes
so much longer relative to any parsing we do. 

## parent-forum on forum-paginator

### original note
paginator fetch on forum should insert this-forum into the parent-forum attribute of each submission belonging to that paginator.
It might be worth deciding whether we are going to have a parent-paginator attribute when there is no forum but this might complicate
things. 

### what has been done
There is now a parent-forum attribute that remains nil when there is no known object for the parent forum.
Maybe there should be a workaround like %comment-get-context% but %submission-get-forum% to make the object.
When the accessor is used if there is none. The problem with this is what if there exists an object for the forum
already and we do not know about it. Therefor we need some way of keeping track of all forum objects. That the user
has a reference to. 

### why I'm now not doing this and removing it from the library
the user could store the submission objects somewhere else and forget about the forum paginator
this means that the paginator could be in a stale state, ie left in page 20 and in order "top"
meaning the output would be completely unexpected. (as they would be expecting them sorted by new)

the same effect could also be achieved just by doing something like this:
`(make-forum-paginator (forum submission) :sorting :new)`

## stuff like body-expand shouldn't be external
they should execute when the reader for e.g. article-body is used
same for replies. 

### solution 
removed body-expand as it is now replaced by `:before` methods for
article-body using submission-refresh.


## make article-get-replies internal
article-get-replies is now internal

## voting with 1 instead of "1" causes an error.
added `fix-choice` function.

## make article-id integer not a string.
This was up for debate

### what was done
It was decided to change the id to an integer because it would be easier to
compare the age of two articles using the <= operators than if they were a string.

## method names
the methods don't need to have the class name appended to them, with the exception of maybe
paginator methods. 

### what was done
several methods had the prefix removed, however a few remain.

`paginator` methods still have the prefix, this is because it isn't vague what a paginator is and
all the methods can be used across different types of paginator.

`article-delete` had to retain the prefix because otherwise it would clash with `cl:delete`. 
There might be a better name but I haven't thought of it yet.

`articles-find-if`, `replies-find-if` and similar methods retained their prefixes for the same reason
as `article-delete` but also for clarity. Maybe `remove-if-not` should be changed to filter but this isn't
really a huge issue.


## reading / navigation
an abstraction above the dom to provide a means of navigation
an example would be using objects to represent submissions and comments

### what was done
The introduction of `article` `comment` `submission` `paginator` classes
and their children.
