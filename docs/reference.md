# CL-POSTPUMP

## special \*DEFAULT-ACCEPT\*
string
Specifies the default value for the **accept** parameter in all http requests.

## special \*DEFAULT-ADDITIONAL-HEADERS\*
assoc list
Specifies any parameters you want to appear in all http requests

## special \*DEFAULT-USER-AGENT\*
string
Specifies the user-agent to use in all http requests

## special \*POSTMILL-SERVER\*
string
Specifies the hostname of the postmill server to send all http requests to

## special missing
internal symbol
denotes when the information for this slot has not been downloaded from the postmill instance yet.
should never be visible through accessors externally.

---

## struct timestamp

### slot datetime
string
a copy of the time format postmill has for an article

### slot title
string
the alphanumeric version of the datetime

---

## class article
base class for both `submission` and `comment` classes.

### slot id
The id of the article as it appears on postmill e.g. the url number in '/cv/4324'

### accessor (ID OBJECT) 

### slot score
The score of the article.

### accessor (SCORE OBJECT)

### slot time
ppump:timestamp
the timestamp for when the article was posted.

### accessor (TIME-POSTED OBJECT)

### slot body
string
The body from the article on postmill serialised into a string.

### accessor (BODY OBJECT)

### slot url
string
the relative url to the article e.g. '/2564' or '2564/comment/3902'

### accessor (PERMALINK OBJECT)

### slot replies
vector or 'missing (internal symbol)
a vector containing article objects representing the articles children e.g. the top level comments
for a submission or the replies to a comment.

### accessor (REPLIES OBJECT)

### generic (GET-VOTE-TOKEN THIS-ARTICLE COOKIE-JAR)
**INTERNAL**

Returns a string containing the csrf token needed to vote.

### generic (VOTE CHOICE THIS-ARTICLE COOKIE-JAR)
vote on the article using the user logged in with the cookie-jar

choice can be either 1, 0 or -1 in either an integer, string or keyword
choice also takes `:up` `:down` `:no` as well as `:upvote`  `:downvote` `:no-vote`

see function fix-choice.

### generic (REPLY THIS-ARTICLE COMMENT-BODY COOKIE-JAR &KEY CONTEXT)

posts a reply to the supplied article containing the string `comment-body` using the user
logged in with `cookie-jar`.

Context is a boolean defaulted to `nil` when an non nil value is supplied to context, the result
will be pushed to the `replies` of `this-article`.

### generic (ARTICLE-DELETE THIS-ARTICLE COOKIE-JAR)
sends a post request to the postmill instance to delete the article using the user logged in with
`cookie-jar`.

### generic (REPLIES-FIND-IF FUNCTION THIS-ARTICLE)
used to find the first comment in the replies to an article matching the predicate.

### generic (REPLIES-REMOVE-IF-NOT FILTER-FN THIS-ARTICLE)
returns a vector containing all the replies to an article matching the predicate supplied.

---

## class comment
inherits article
represents a comment on a postmill instance

### slot parent
`comment`, `nil` or `'missing`
represents a reference to the parent comment of this comment
`nil` when this comment is a top level comment

### accessor (PARENT OBJECT)

### slot parent-submission
`submission` or `missing`
represents the submission the comment was posted in.

### accessor (PARENT-SUBMISSION OBJECT)

### method (%COMMENT-GET-CONTEXT% THIS-COMMENT)
**INTERNAL**
requests the comment from the postmill instance in order to update the attributes that are marked as
`'missing`, this is usually needed when a comment has originated from a `comment-pagiantor`.

It should only be used by accessors using `:before` which is why it is marked with `%%`.

### method (UPDATE-INSTANCE-WITH-INSTANCE TO-UPDATE TO-USE)
**INTERNAL**
reinitializes the `to-update` object with the slot values of the `to-use` object.

---

## class submission
inherits article
represents a submission from a postmill instance.

### slot link
string
the url posted with the submission, is a relative url to itself when the submission is a "self post"

### accessor (SUBMISSION-LINK OBJECT)

### slot forum
string
relative url to the forum the submission was posted to.

### accessor (SUBMISSION-FORUM OBJECT)

### slot title
string
the title posted with the submission.

### accessor (SUBMISSION-TITLE OBJECT)

### method (REFRESH-SUBMISSION THIS-SUBMISSION)
#### INTERNAL
used to get attributes that accessors cant' access when they are missing by fetching the
submission resource from the postmill instance via the submission permalink.

should only be used by accessors and used when submissions have originated from a paginator.

### method (SUBMISSION-GET-REPLIES THIS-ARTICLE &KEY PARSED-PAGE)
#### INTERNAL
unless `parsed-page` is provided, sends a post request for the article replies, parses them and adds them
to `this-article`'s replies.

when `parsed-page`is provided, parses the plump element given in parsed-page and adds the replies to
`this-article`'s replies.

only used by `refresh-submission` and `make-submission`

### function (MAKE-SUBMISSION URL)
returns a submission object complete with replies using the relative url provided as string.

---

## class paginator
base class for `submission-paginator` and `comment-paginator`

### slot feed
string
relative url representing the pagination url e.g. '/comments/5' or '/f/hacking/?hot&2090'

### accessor (PAGINATOR-FEED OBJECT)
internal

### slot articles
vector
the parsed articles for the current page of pagination.

### accessor (PAGINATOR-ARTICLES OBJECT)

### generic (PAGINATOR-FETCH THIS-PAGINATOR &KEY COOKIE-JAR)
requests a page with the value of `(paginator-feed this-paginator)`, parses the articles and stores them
in in the paginators `'articles` slot.

updates `paginator-feed` to point at the next page.

### generic (PAGINATOR-ARTICLE THIS-PAGINATOR INDEX)
returns the article at the index in the paginators `'articles` slot.

### generic (PAGINATOR-FIND-IF PREDICATE THIS-PAGINATOR END-ID)
will search the paginator feed until the first article in the paginator that matches the predicate is
found or the end-comment-id is reached.

end-comment-id is not a keyword argument for verification reasons. 

### generic (PAGIANTOR-REMOVE-IF-NOT PREDICATE THIS-PAGINATOR END-ID)
will search the paginator feed until the end-comment-id and return a vector containing all the articles
matching the predicate.

### generic (PAGINATOR-REGEX REGEX THIS-PAGIANTOR END-ID &KEY KEY)
will search the paginator feed until the end-id for all articles matching the regular expression.
`:key` is the method to use to get a valid string to test the regex against.
is `#'title` by default for submission paginators and `#'body` for comment paginators.

## class comment-paginator
inherits `paginator`
to be used for '/comments' or '/user/comments'

## class submission-paginator
inherits `paginator`
to be used for any url where submission pagination occurs such as the front page or any forum.

### function (MAKE-SUBMISSION-PAGINATOR URL &KEY SORTING)
returns a submission-paginator for the url with the sorting type applied if provided.

`:sorting` accepts `:hot`, `:new`, `:top`, `:controversial` and `:most_commented`.

the paginator will not apply sorting to the url before initialising a feed if none is provided.

---

## macro (HTTP-200-OF-FAIL &BODY BODY)
**INTERNAL**
raises an error if the drakma http request passed in the body does not return a status of `200`.

## function (GET-REQUEST URL &KEY USER-AGENT ACCEPT COOKIE-JAR ADDITIONAL-HEADERS REFERER)
uses drakma to create a get request on the resource specified in the string `url`

`:user-agent` a string for the user-agent, \*DEFAULT-USER-AGENT\* by default.
`:accept` a string for an accept header, \*DEFAULT-ACCEPT\* by default.
`:cookie-jar` a cookie-jar to use with the request.
`:additional-headers` \*DEFAULT-ADDITIONAL-HEADERS\* by default. An assoc list e.g.
`'(("Accept-Language" . "en-US,en"))`
`:referer` a referer url string to send with the request, is appended to additional headers.

see the docs on `drakma:http-request` for more information

## function (POST-REQUEST URL &KEY USER-AGENT ACCEPT COOKIE-JAR ADDITIONAL-HEADERS REFERER PARAMETERS)

`:user-agent` a string for the user-agent, \*DEFAULT-USER-AGENT\* by default.
`:accept` a string for an accept header, \*DEFAULT-ACCEPT\* by default.
`:cookie-jar` a cookie-jar to use with the request.
`:additional-headers` \*DEFAULT-ADDITIONAL-HEADERS\* by default. An assoc list e.g.
`'(("Accept-Language" . "en-US,en"))`
`:referer` a referer url string to send with the request, is appended to additional headers.
`:parameters` an assoc list of parameters to post with the request. e.g.
`(("\_csrf\_token" . "dfkldfj") ("_choice" . "1"))`

see the docs on `drakma:http-request` for more information

## macro (DO-REGEX ITEM)
**INTERNAL**
used to simplify a call to cl-ppcre with only one capture group.

## function (MAKE-URL &REST BITS)
**INTERNAL**
used to append the bits to the hostname so it can be used with http requests
e.g. make a url absolute.

## function (LOGIN USERNAME PASSWORD COOKIE-JAR &KEY REFERER)
logs into the postmill instance and stores the cookies for the session in the cookie-jar
username and password should both be strings.
`:referer` the referer url to be sent with the post request. See `post-request`.

## function (FIX-CHOICE CHOICE)
**INTERNAL**
converts a range of values to one that is acceptable in in the parameters for voting.
accepts 1, :1, :up, :upvote, -1, :-1, :down, :downvote, 0, :0 :no :no-vote
and produces "1", "-1" or "0".

## function (VOTE-SUBMISSION CHOICE POST-URL COOKIE-JAR)
posts the choice to the post-url and fetches the csrf token.

`choice` can be any value accepted by `fix-choice`.
`post-url` must be an absolute url to the article.
`cookie-jar` must be a `'drakma:cookie-jar` with a cookie for an active session.

please see generic `vote` if you are using the article objects.

## function (VOTE-COMMENT CHOICE URL COOKIE-JAR)
posts the choice to the url and fetches the csrf token.

`choice` can be any value accepted by `fix-choice`.
`url` must be an absolute url to the article.
`cookie-jar` must be a `'drakma:cookie-jar` with a cookie for an active session.

please see generic `vote` if you are using the article objects.

## function (POST-SUBMISSION COOKIE-JAR SUBMISSION-TITLE SUBMISSION-FORUM &KEY SUBMISSION-BODY)
posts a submission to a forum.

`cookie-jar` must be a `'drakma:cookie-jar` with a cookie for an active session.
`submission-title` a string containing the title for the submission
`submission-forum` accepts the name of the forum as string, not the relative url.
`:submission-body` a body to send with the submission.

## function (GET-REPLY-TOKEN COOKIE-JAR)
**INTERNAL**
returns the csrf token needed to post a reply.

## function (POST-COMMENT URL COMMENT-BODY COOKIE-JAR &KEY REFERER COMMENT-EMAIL)
post a reply to the article at the `url` using `comment-body` and the cookie-jar

`:referer` is the url to be redirected to by the webserver upon success.
`:comment-email` is a hidden <input> that is sent with the forms to postmill, your post-request
should fail if this is set to anything but an empty string, which it is by default.

## function (DELETE-COMMENT URL COOKIE-JAR)
deletes the comment at the url on the postmill instance.

`url` relative url to the comment as string

please see generic `article-delete`.

## function (PARSE-TO-COMMENT ARTICLE &KEY PARENT PARENT-SUBMISSION)
uses a plump element containing an `<article>` tag and it's children to return a comment object.

`:parent` a parent comment to use as the `parent` attribute. 'missing by default.
`:parent-submission` the parent submission to use for the `parent-submission` attribute.
'missing by default.

see class `comment`.


## function (RECURSE-COMMENT REPLY-DIV PARENT &KEY REPLIES PARENT-SUBMISSION)
**INTERNAL**
returns all the replies to in the reply-div as comment objects in a vector.
called by parse-to-comment.

`reply-div` a plump element containing ".comment-replies" div for a comment.
`parent` the `comment` to use as the parent for all the replies.
`:replies` what value to return should the div have no children.
`:parent-submission` the `submission` object to use as the `parent-submission` for all the replies.

see class `comment`, function `parse-to-comment`.

## function (PARSE-TO-SUBMISSION ARTICLE &KEY THIS-ARTICLE)
returns a submission object from a plump element containing an `<article>` tag

`:this-article` when a submission object is provided, the submission object will be reinitialized
with the information parsed from the plump element in `article`.

see class `submission`.

## function (PARSE-SUBMISSION-BODY ARTICLE)
**INTERNAL**
returns a string for the submission body with the plump element supplied by `article`

## function (ARTICLES-REMOVE-IF-NOT PREDICATE ARTICLES &KEY END-ID)
find all the article instances satisfying the `predicate` in the supplied vector `articles`.

`:end-id` if supplied then the search will stop if the end-id is reached.

returns two values, the vector for the result and a boolean that is true if the end was not reached.

see `paginator-remove-if-not`, class `article`.

## function (ARTICLES-FIND-IF PREDICATE ARTICLES &KEY END-ID)
find the first article matching the predicate in the supplied vector `articles`

`:end-id`, when supplied the search will stop if the end-id is reached.

returns two values, the result and a boolean that is true if the end was not reached.

see `paginator-find-if`, class `article`.

## function (ARTICLES-REGEX REGEX ARTICLES END-COMMENT-ID &KEY KEY)
Not really stable, should be internal.

returns all the article instances which match the supplied regular expression `regex`

see `paginator-regex`, class `article`.

