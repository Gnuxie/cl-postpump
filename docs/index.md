# cl-postpump
### <u>For users</u>:
  * Please read the [**readme**](../README.md) for installation and getting started.
  * [**Examples**](./examples.md) of usage
  * [**Reference**](./reference.md)


### <u>For developers:
* see my [**notes**](./todo.md) (just a todo atm)
  * [**Reference**](./reference.md)

## Dependencies 
* **drakma**
information about drakma can be found on the [github site](https://edicl.github.io/drakma/), [quicklisp.org](http://quickdocs.org/drakma/api) or [common-lisp.net](https://common-lisp.net/~loliveira/ediware/drakma/doc/)
* **plump**
information about plump can be found on the [github site](https://shinmera.github.io/plump/)
* **lquery**
information about lquery can be found on the [github site](https://shinmera.github.io/lquery/) additionally [lisp cookbook](https://lispcookbook.github.io/cl-cookbook/web-scraping.html) has a nice getting started guide on using lquery
* **cl-ppcre**
information about cl-ppcre can be found on the [github site](https://edicl.github.io/cl-ppcre/) which also has a guide on [lisp-cookbook](https://lispcookbook.github.io/cl-cookbook/regexp.html)


## disclaimer
this is my first-ish project, feedback is currently welcome as are suggestions for coding style/practice/etc
feature requests are allowed but I'm also allowed to ignore them :) 


## special thanks to:
[Bea](https://gitlab.com/beabee), [Jaided](https://notabug.org/jadedctrl), [Theemacsshibe](https://gitlab.com/Theemacsshibe) and #clschool on freenode
