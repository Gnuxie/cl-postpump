(defpackage :cl-postpump
  (:use :cl)
  (:nicknames :ppump)
  (:export
   :*default-user-agent*
   :*default-accept*
   :*default-additional-headers*
   :*postmill-server*

   :comment
   :submission



   :paginator-articles
   :paginator-article
   :paginator-fetch
   :paginator-regex
   :paginator-find-if
   :paginator-remove-if-not
   
   :comment-paginator
   :submission-paginator

   :make-submission
   :make-submission-paginator

   :body
   :submission-title
   :article-delete
   :score
   :id
   :username
   :time-posted
   :replies
   :articles-find-if
   :articles-remove-if-not
   :replies-remove-if-not
   :replies-find-if

   :parent-submission
   :parent
   
   :vote

   :reply

   :missing
   :get-request
   :post-request
   :login
   :vote-submission
   :vote-comment
   :post-submission
   :delete-submission
   :post-comment
   :delete-comment
   :parse-to-comment
   :parse-to-submission))
(in-package :cl-postpump)
