(in-package :cl-postpump)

(defclass comment (article)
  ((parent
    :initarg       :parent
    :initform      'missing
    :accessor      parent
    :type          :comment
    :documentation "a reference to the parent comment")
   (parent-submission
    :initarg       :parent-submission
    :initform      'missing
    :accessor      parent-submission
    :type          :submission
    :documentation "a reference to the parent submission")))

(defmethod get-vote-token ((this-article comment) cookie-jar)
  (let ((selector (concatenate 'string "#comment_"
			       (write-to-string (id this-article))
			       " input[name\"token\"]"))
	(parsed-page (get-request (make-url (permalink this-article))
				  :cookie-jar cookie-jar)))
    (elt (lquery:$ parsed-page selector (attr :value)) 0)))

(defmethod vote (choice (this-article comment) cookie-jar)
  (let ((action-token (get-vote-token this-article cookie-jar)))
    (post-request (make-url "/cv/" (write-to-string (id this-article)))
		  :cookie-jar cookie-jar
		  :parameters `(("token"  . ,action-token)
				("choice" . ,(fix-choice choice))))))

(defmethod article-delete ((this-article comment) cookie-jar)
  (let ((url (permalink this-article)))
    (delete-comment url cookie-jar)))

(defmethod %comment-get-context% ((this-comment comment))
  "request the context for the comment, build a new submission object
   and then return a new comment that is a child of the submission object.
   Used when a comment has originated from a comment-paginator."
  (let* ((parent-submission-url (do-regex "(\\/f\\/[a-zA-z]+\\/\\d+)\\/comment" (permalink this-comment)))
	 (new-submission (make-submission parent-submission-url))
	 (new-comment (replies-find-if #'(lambda (a-comment)
					   (= (id a-comment)
					      (id this-comment)))
				       new-submission)))

    (update-instance-with-instance this-comment new-comment)))

(defmethod parent-submission :before ((this-comment comment))
  "used as a reader for the parent-submission attribute of a comment"
  (when (equal 'missing (slot-value this-comment 'parent-submission)) (%comment-get-context% this-comment)))

(defmethod parent :before ((this-comment comment))
  "Used as a reader for the parent-comment attribute of a comment"
  (when (equal 'missing (slot-value this-comment 'parent)) (%comment-get-context% this-comment)))

(defmethod replies :before ((this-article comment))
  (when (equal 'missing (slot-value this-article 'replies))
    (%comment-get-context% this-article)))

(defmethod update-instance-with-instance ((to-update comment) (to-use comment))
  (reinitialize-instance to-update
			 :id                (slot-value to-use 'id)
			 :score             (slot-value to-use 'score)
			 :url               (slot-value to-use 'url)
			 :username          (slot-value to-use 'username)
			 :time              (slot-value to-use 'time)
			 :body              (slot-value to-use 'body)
			 :replies           (slot-value to-use 'replies)
			 :parent            (slot-value to-use 'parent)
			 :parent-submission (slot-value to-use 'parent-submission)))
