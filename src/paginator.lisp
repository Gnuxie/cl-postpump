(in-package :cl-postpump)

(defclass paginator ()
  ((feed
    :initarg       :feed
    :initform      (error "Must supply a feed url RELATIVE")
    :accessor      paginator-feed
    :type          string
    :documentation "A relative url to fetch articles from")
   (articles
    :initarg       :articles
    :initform      #()
    :accessor      paginator-articles
    :type          vector
    :documentation "A list of article objects paginator  has")))

;;; AN INDIVIDUAL SUBMISSION DOES NOT HAVE PAGINATION IN THE COMMENTS so far only /user/comments and /comments is there a use case
(defclass comment-paginator (paginator) ())

;;; articles in submission paginator don't have a body, they have a title
;;; NOT AN INDIVIDUAL SUBMISSION
(defclass submission-paginator (paginator) ())
;;; submission will need to have an expander method which then fetches replies and body

(defgeneric paginator-article (this-paginator index)
  (:documentation "return the article at index"))

(defmethod paginator-article ((this-paginator paginator) index)
  (elt (paginator-articles this-paginator) index))

(defgeneric paginator-fetch (this-paginator &key cookie-jar)
  (:documentation "fetches articles from the feed and stores them in the paginator 'articles slot"))

(defmethod paginator-find-if (predicate (this-paginator paginator) end-id)
  (loop with result = nil
     with go-again = t
     do (unless (and (not result) go-again)
	  (return result))
       (multiple-value-bind (next-result next-go-again)
	   (articles-find-if predicate (paginator-articles this-paginator) :end-id end-id)
	 (setf go-again next-go-again)
	 (setf result next-result)
	 (paginator-fetch this-paginator))))

(defmethod paginator-remove-if-not (predicate (this-paginator paginator) end-id)
  (loop with result-acc = (make-array 25 :fill-pointer 0)
     with go-again = t
     do (unless go-again
	  (return result-acc))
       (multiple-value-bind (result next-go-again)
	   (articles-remove-if-not predicate (paginator-articles this-paginator) :end-id end-id)
	 (setf go-again next-go-again)
	 (setf result-acc (concatenate 'vector result-acc result))
	 (paginator-fetch this-paginator))))

(defgeneric paginator-regex (regex this-paginator end-id &key key))

(defmacro define-paginator-regex (paginator-class default-key)
  `(defmethod paginator-regex (regex (this-paginator ,paginator-class) end-id &key (key ,default-key))
     (loop with result-acc = (make-array 25 :fill-pointer 0)
     with go-again = t
     do (unless go-again
	  (return result-acc))
       (multiple-value-bind (result next-go-again)
	   (articles-regex regex (paginator-articles this-paginator) end-id :key key)
	 (setf go-again next-go-again)
	 (setf result-acc (concatenate 'vector result-acc result))
	 (paginator-fetch this-paginator)))))

(define-paginator-regex comment-paginator #'body)
(define-paginator-regex submission-paginator #'submission-title)

(defclass forum (submission-paginator)
  ((name
    :initarg       :name
    :initform      (error "Must supply a forum name")
    :accessor      forum-name
    :type          string
    :documentation "The name of the forum")

   (url
    :initarg       :url
    :initform      (error "Must supply a forum url")
    :accessor      forum-url
    :type          string
    :documentation "The relative URL of the forum")))

(flet ((add-sorting (url keyword)
	 (concatenate 'string url
		      (cond ((equal keyword :hot)            "")
			    ((equal keyword :new)            "/new")
			    ((equal keyword :top)            "/top")
			    ((equal keyword :controversial)  "/controversial")
			    ((equal keyword :most_commented) "/most_commented")
			    (t "")))))

  (defun make-forum (url &key (sorting :hot))
    (let ((forum-name (subseq url 3))
	  (feed (add-sorting url sorting)))
      (make-instance 'forum :feed feed :url feed :name forum-name)))

  (defun make-submission-paginator (url &key sorting)
    (when (string= url "/") (setf url ""))
    (make-instance 'submission-paginator :feed (add-sorting url sorting))))

(flet ((article-update (translate-article this-paginator &key cookie-jar)
	 (let ((parsed-page (get-request (make-url (paginator-feed this-paginator))
				      :cookie-jar cookie-jar)))
	   (let ((article-elements (lquery:$ parsed-page "main article")))
	  
	      (setf (paginator-articles this-paginator)
	            (map 'vector #'(lambda (x) (funcall translate-article x)) article-elements))

	      (let ((next-page-url (lquery:$ parsed-page ".next a" (attr "href"))))
                (when (not (equal (length next-page-url) 0))
		  (setf (paginator-feed this-paginator)
			(elt next-page-url 0))))))))
  
  (defmethod paginator-fetch ((this-paginator comment-paginator) &key cookie-jar)
    (article-update #'parse-to-comment this-paginator :cookie-jar cookie-jar))

  (defmethod paginator-fetch ((this-paginator submission-paginator) &key cookie-jar)
    (article-update #'parse-to-submission this-paginator :cookie-jar cookie-jar)))
