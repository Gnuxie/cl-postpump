(in-package :cl-postpump)

(defclass article ()
    ((id
      :initarg :id
      :initform (error "Must supply a article id")
      :accessor id
      :type :integer
      :documentation "The id of the article e.g. the url number in '/cv/4324'")
     (score
      :initarg :score
      :initform (error "Must supply a article score")
      :accessor score
      :type :string
      :documentation "The score of the article")
     (username
      :initarg :username
      :initform (error "Must supply a article username")
      :accessor username
      :type :string
      :documentation "The username that is of the article author")
     (time
      :initarg :time
      :initform (error "Must supply a article timestamp")
      :accessor time-posted
      :type :timestamp
      :documentation "A time struct representing the time the article was created")
     (body
      :initarg :body
      :initform (error "Must supply a article body")
      :accessor body
      :type :string)
     (url
      :initarg :url
      :initform (error "Must supply article url")
      :accessor permalink
      :type :string
      :documentation "A relative URL to the article")
     (replies
      :initarg :replies
      :initform 'missing
      :accessor replies
      :type :vector
      :documentation "A list containing the replies")))

(defgeneric get-vote-token (this-article cookie-jar)
  ;; private - do not export
  (:documentation "return an action token to vote on this-article using the user in cookie-jar"))

(defgeneric vote (choice this-article cookie-jar)
  ;; public
  (:documentation "vote on this-article with choice using the user logged in with cookie-jar, see fix-choice."))

(defgeneric article-delete (this-article cookie-jar)
  (:documentation "used to delete an article on the site (not the object)"))

(defgeneric reply (this-article comment-body cookie-jar &key context)
  (:documentation "reply to an article (post a comment) using the cookie-jar
                  should return the new comment as an object.
                  :context if a non new value is provided then the new comment will be pushed to
                  this-articles replies"))


(defmethod reply ((this-article article) comment-body cookie-jar &key (context nil))
  (let* ((parsed-page (plump:parse (post-comment (permalink this-article)
						 comment-body
						 cookie-jar
						 :referer (permalink this-article))))
	 ;; this uses "article[class*=comment]" because lquery:$ has a bug where it doesn't recognise the line breaks properly
	 ;; between the other class names in the article so don't use ".comment"!!!!
	 (new-comment (parse-to-comment (lquery:$1 parsed-page "article[class*=comment]"))))
    
    (if context
	(push (replies this-article) new-comment)
	new-comment)))

(defmethod replies-find-if (predicate (this-article article))
  ;; needs a better solution than this.
  (let ((results (replies-remove-if-not predicate this-article)))
    (if (= 0 (length results)) #() (elt results 0))))

(defmethod replies-remove-if-not (filter-fn (this-article article))
  ;; breadth first search
  (flet ((all-children (replies)
           (reduce (lambda (acc comment) (concatenate 'vector acc (replies comment)))
		   replies :initial-value #()))
       
	 (filter-comment-vector (comments)
	   (remove-if-not filter-fn comments)))
	   
    (loop with result-acc = #()
          with comments = (replies this-article)
          do
         (if (= (length comments) 0)
             (return result-acc)
             (setf result-acc (concatenate 'vector result-acc
					   (filter-comment-vector comments))
		   comments (all-children comments))))))
