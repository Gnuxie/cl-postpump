(in-package :cl-postpump)

(defclass submission (article)
  ((link
    :initarg       :link
    :initform      (error "Must supply a submission link")
    :accessor      submission-link
    :type          :string
    :documentation "The a URL posted with the submission, is relative when self post")
   (forum
    :initarg       :forum
    :initform      (error "Must supply a submission forum")
    :accessor      submission-forum
    :type          :string
    :documentation "The relative url to the forum this submission was posted to")
   (title
    :initarg       :title
    :initform     (error "Must supply a submission title")
    :accessor      submission-title
    :type          :string
    :documentation "The title appearing over the link")))

(defun make-submission (url)
  "returns a new submission object from the url with all it's replies"
  (let* ((page (get-request (make-url url)))
	 (submission (parse-to-submission (lquery:$1 page ".submission"))))
    (submission-get-replies submission :parsed-page page)
    submission))

(defmethod article-delete ((this-article submission) cookie-jar)
  (let ((url (permalink this-article)))
    (delete-submission url cookie-jar)))

(defmethod vote (choice this-article cookie-jar)
  (vote-submission choice (permalink this-article) cookie-jar))

(defmethod submission-get-replies ((this-article submission) &key parsed-page)
  (let* ((page (if parsed-page
		   parsed-page
		   (get-request (make-url (permalink this-article)))))
	 
	 (comments (plump:child-elements (lquery:$1 page ".submission-comments"))))
    
    (setf (replies this-article)
	  (map 'vector #'(lambda (x)
			   (parse-to-comment x
					     :parent nil
					     :parent-submission this-article))
	       comments))))

(defmethod refresh-submission ((this-submission submission))
  "used to get attributes that accessors can't access when they are missing
   by parsing the page at the permalink of the submission"
    (let ((page (get-request (make-url (permalink this-submission)))))
    (parse-to-submission (lquery:$1 page ".submission") :this-article this-submission)
    (submission-get-replies this-submission :parsed-page page)
    this-submission))

(defmethod body :before ((this-article submission))
  (when (equal 'missing (slot-value this-article 'body))
    (refresh-submission this-article)))

(defmethod replies :before ((this-article submission))
  (when (equal 'missing (slot-value this-article 'replies))
    (refresh-submission this-article)))
