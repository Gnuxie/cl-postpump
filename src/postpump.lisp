(in-package :cl-postpump)

(defvar *postpump-version* "0.2.0")
(defvar *default-user-agent* (concatenate 'string "cl-postpump/" *postpump-version*
					  " (drakma " drakma:*drakma-version* ")"))
(defvar *default-accept* "text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8")
(defvar *default-additional-headers* '(("Accept-Language" . "en-US,en;q=0.5")))
(defvar *postmill-server* "https://community.postmill.xyz")

(defstruct timestamp
  datetime
  title)

(defmacro http-200-or-fail (&body body)
  ;; response-body status headers uri stream close-stream reason
  (let ((response-body (gensym)) (status (gensym)) (headers (gensym)) (uri (gensym)) (stream (gensym)) (close-stream (gensym)) (reason (gensym)))
    `(destructuring-bind (,response-body ,status ,headers ,uri ,stream ,close-stream ,reason)
	 (multiple-value-list (progn ,@body))
       (if (/= ,status 200)
	   (error "HTTP request returned ~d" ,status)
	   (values ,response-body ,status ,headers ,uri ,stream ,close-stream ,reason)))))

(defun get-request (url &key
			  (user-agent *default-user-agent*)
			  (accept *default-accept*)
			  cookie-jar
			  (additional-headers *default-additional-headers*)
			  (referer nil))

  (let ((headers (if referer
		   (append `(("Referer" . ,referer)) additional-headers)
		   additional-headers)))

    (plump:parse (http-200-or-fail (drakma:http-request url
							:user-agent         user-agent
							:accept             accept
							:cookie-jar         cookie-jar
							:additional-headers headers)))))


(defun post-request (url &key
			   (user-agent *default-user-agent*)
			   (accept *default-accept*)
			   cookie-jar
			   (additional-headers *default-additional-headers*)
			   (referer nil)
			   parameters)

  (let ((headers (if referer
		   (append `(("Referer" . ,referer)) additional-headers)
		   additional-headers)))
    
    (http-200-or-fail (drakma:http-request url
					   :method             :post
					   :user-agent         user-agent
					   :accept             accept
					   :cookie-jar         cookie-jar
					   :additional-headers headers
					   :parameters         parameters))))

(defmacro do-regex (regex item)
  `(elt (nth-value 1 (ppcre:scan-to-strings ,regex ,item)) 0))

(defun make-url (&rest bits)
  (format nil "~a~{~a~}" *postmill-server* bits))

(defun login (username password cookie-jar &key (referer nil))
  (flet ((grab-csrf (page)
	   (elt (lquery:$ page "input[name=\"_csrf_token\"]" (attr :value)) 0)))
    (let ((csrf_token (grab-csrf (get-request (make-url "/login")
					      :cookie-jar cookie-jar
					      :referer referer))))
      (post-request (make-url "/login_check")
		    :cookie-jar cookie-jar
		    :parameters `(("_csrf_token" . ,csrf_token)
				  ("_username"   . ,username)
				  ("_password"   . ,password))
		    :referer referer))))


(defun fix-choice (choice)
  (case choice
    (1 "1")
    (:1 "1")
    (:up "1")
    (:upvote "1")
    (-1 "-1")
    (:-1 "-1")
    (:down "-1")
    (:downvote "-1")
    (0 "0")
    (:0 "0")
    (:no "0")
    (:no-vote "0")
    (otherwise
      (if (and (typep choice 'string) (or (string= choice "1") (string= choice "-1") (string= choice "0")))
        choice
        (error "~S is not a possible choice value." choice)))))


(defun vote-submission (choice post-url cookie-jar)
  (let ((parsed-page (get-request post-url :cookie-jar cookie-jar)))
    (let ((action-token (elt (lquery:$ parsed-page "input[name=\"token\"]" (attr :value)) 0))
	  (action-id (do-regex (if (search "/f/" post-url)
				     ".*[a-zA-Z]*\\/(\\d*)\\/"
				     ".*\\/(\\d*)\\/")
		       post-url)))
      (post-request (make-url "/sv/" action-id) :cookie-jar cookie-jar
		    :parameters `(("token"  . ,action-token)
				  ("choice" . ,(fix-choice chioce)))))))

(defun vote-comment (choice url cookie-jar)
  (let* ((comment-id (do-regex ".*comment/([0-9]*)" url))
	 (article-id (concatenate 'string "#comment_" comment-id))
	 (parsed-page (get-request url :cookie-jar cookie-jar))
	 (article (lquery:$ parsed-page article-id))
	 (action-token (elt (lquery:$ article "input[name=\"token\"]" (attr :value)) 0)))

    (post-request (make-url "/cv/" comment-id)
		  :cookie-jar cookie-jar
		  :parameters `(("token"  . ,action-token)
				("choice" . ,(fix-choice choice))))))

(defun post-submission (cookie-jar submission-title submission-forum &key (submission-body "")
								       (submission-url ""))
  (let* ((submit-page (get-request (make-url "/submit/" submission-forum) :cookie-jar cookie-jar))
	 (submission-token (elt (lquery:$ submit-page "#submission__token" (attr :value)) 0))
	 (forum-id (elt (lquery:$ submit-page "option[selected]" (attr :value)) 0)))

    (post-request (make-url "/submit")
		  :cookie-jar cookie-jar
		  :parameters `(("submission[url]"    . ,submission-url)
				("submission[title]"  . ,submission-title)
				("submission[body]"   . ,submission-body)
				("submission[forum]"  . ,forum-id) ;warning this is a number
				("submission[submit]" . "")
				("submission[email]"  . "")
				("submission[_token]" . ,submission-token)))))

(defun delete-submission (url cookie-jar)
  (let ((parsed-page (get-request url :cookie-jar cookie-jar))
	(forum-name (do-regex "/f/([a-zA-Z]*)" url))
	(post-id (do-regex (if (search "/f/" url)
			       ".*[a-zA-Z]*\\/(\\d*)\\/"
			       ".*\\/(\\d*)\\/")
		   url)))
    (let ((action-token (elt (lquery:$ parsed-page ".submission-nav 
                                                    input[name=\"token\"]" (attr :value)) 0)))
      (post-request (make-url "/f/" forum-name "/delete_submission/" post-id)
		    :cookie-jar cookie-jar
		    :parameters `(("token" . ,action-token))))))

(defun get-reply-token (cookie-jar)
  "return an action token for posting a comment using the cookie jar"
  (let ((parsed-page (get-request (make-url "/comment_form/0/0/0") :cookie-jar cookie-jar)))
    (elt (lquery:$ parsed-page "#comment__token" (attr :value)) 0)))

(defun post-comment (url comment-body cookie-jar &key (referer nil) (comment-email ""))
  "post a reply to the article at the `url` using `comment-body` and the cookie-jar
   :referer is the url to be redirected to by the webserver upon success.
   :comment-email is a hidden <input> that is sent with the forms to postmill, your post-request
   should fail if this is set to anything but an empty string."
  (let ((action-url (if (search "comment" url)
			(concatenate 'string
				     (do-regex ".*(/f/.*)\/comment" url)
				     "/comment_post/"
				     (do-regex "\/comment\/(\\d*)" url))
			(concatenate 'string
				     (do-regex ".*(/f/.*\\d*)\/" url)
				     "/comment_post")))
	
	(action-token (get-reply-token cookie-jar)))
    (post-request (make-url action-url)
		  :cookie-jar cookie-jar
		  :parameters `(("comment[comment]" . ,comment-body)
				("comment[submit]"  . "")
				("comment[email]"   . ,comment-email)
				("comment[_token]"  . ,action-token))
		  :referer referer)))

(defun delete-comment (url cookie-jar)
  (let ((parsed-page (get-request (make-url url) :cookie-jar cookie-jar))
	(action-url (concatenate 'string (do-regex "(/f/.*\\d*)/comment" url)
				 "/delete_comment/"
				 (do-regex "/comment/(\\d*)" url))))
    (let* ((form-selector (concatenate 'string "form[action=\""
				       action-url "\"] > input[name=\"token\"]"))
	   (action-token (elt (lquery:$ parsed-page form-selector (attr :value)) 0)))
      (post-request (make-url action-url)
		    :cookie-jar cookie-jar
		    :parameters `(("token" . ,action-token))))))

(defun parse-to-comment (article &key (parent 'missing) (parent-submission 'missing))
  "helper function to convert a <article> tag and it's children to a comment object"
  (let ((id (parse-integer (subseq (lquery:$1 article         (attr "id")) 8)))
	(score      (lquery:$1 article "form"                 (attr "data-score")) )
	(url        (lquery:$1 article ".comment-nav-reply a" (attr "href")) )
	(username   (lquery:$1 article ".comment-user"        (attr "href")) )
	(datetime   (lquery:$1 article "time"                 (attr "datetime")) )
	(time-title (lquery:$1 article "time"                 (attr "title")) )
	(body       (let ((comment-body (lquery:$1 article ".comment-inner")))
		      (plump:SERIALIZE (lquery:$ comment-body ".comment-body > *") nil)))
	(replies    'missing))
	       
    (let ((new-comment (make-instance 'comment
				      :id       id
				      :score    score
				      :url      url
				      :username username
				      :time     (make-timestamp :datetime datetime :title time-title)
				      :body     body
				      :replies  replies
				      :parent   parent
				      :parent-submission parent-submission)))
      (let ((comment-context (lquery:$1 article ".comment__context")))
	(when (not comment-context)
	  (let* ((comment-replies (lquery:$1 article "div[class*=comment-replies]"))
		 (new-replies (recurse-comment comment-replies new-comment :parent-submission parent-submission)))
	    (setf (replies new-comment) new-replies)))
	new-comment))))

(defun recurse-comment (reply-div parent &key (replies #()) (parent-submission 'missing))
    (let ((children (lquery:$ reply-div (children))))
      (if (eql (length children) 0)
	  replies
          (map 'vector #'(lambda (x) (parse-to-comment x :parent parent :parent-submission parent-submission)) children))))

(defun parse-to-submission (article &key this-article)
  "helper function to convert a <article> tag and it's children to a submission object"
  (let ((id (parse-integer (subseq (elt (lquery:$ article "form" (attr "action")) 0) 4)))
	(score      (lquery:$1 article "form"                   (attr "data-score")))
	(url        (lquery:$1  article ".submission-nav a"     (attr "href")))
	(username   (lquery:$1 article ".submission-submitter"  (attr "href")))
	(datetime   (lquery:$1 article "time"                   (attr "datetime")))
	(time-title (lquery:$1 article "time"                   (attr "title")))
	(title      (lquery:$1 article ".submission-link" (render-text)))
	(link       (lquery:$1 article ".submission-link" (attr "href")))
	
	;; body is not displayed during pagination of submissions
	(body (parse-submission-body article))
	(replies    'missing)) ; (parse-replies (lquery:$ "article[class*=comment-top-level]"))
    (let ((forum      (let ((forum-vec (lquery:$ article ".submission-forum" (attr "href"))))
			(if (equal (length forum-vec) 0)
			    (do-regex "\/f\/([^\/]*)\/.*" url)
			    (elt forum-vec 0)))))
      (funcall (if this-article #'reinitialize-instance #'make-instance) (or this-article 'submission)
	       :id       id
	       :score    score
	       :url      url
	       :username username
	       :time     (make-timestamp :datetime datetime :title time-title)
	       :title    title
	       :link     link
	       :forum    forum
	       :body     body
	       :replies  replies))))

(defun parse-submission-body (article)
  (if (lquery-funcs:is article ".submission-has-body") 
      (let ((body-div (lquery:$1 article ".submission-body")))
          (if body-div
              (plump:serialize (plump:child-elements body-div) nil)
              'missing))
      nil))

(defmacro define-articles-filter (name filter-fn)
  `(defun ,name (predicate articles &key (end-id 0))
     (values (,filter-fn predicate articles)
	     (< end-id (id (elt articles (- (length articles) 1)))))))

(define-articles-filter articles-remove-if-not remove-if-not)
(define-articles-filter articles-find-if find-if)

(defun articles-regex (regex articles end-id &key (key #'body))
  "finds all the matches in the vector, returns go-again if it's safe to increment the pagination"
  (articles-remove-if-not #'(lambda (current-article)
			      (ppcre:scan-to-strings regex (funcall key current-article)))
			  articles :end-id end-id))


