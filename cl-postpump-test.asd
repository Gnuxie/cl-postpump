(asdf:defsystem cl-postpump-test
  :name "cl-postpump-test"
  :version "0.2.0"
  :licence "AGPLv3+"
  :author "Gnuxie <Gnuxie@protonmail.com>"
  :depends-on ("cl-postpump")
  :components ((:module "test"
			:components
			((:file "test")))))
