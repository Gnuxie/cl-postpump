# cl-postpump
**cl-postpump** is a library written in common-lisp for interfacing with [**postmill**](https://gitlab.com/edgyemma/Postmill) servers, it can be used to create bots, scrape pages, make clients and more.

## We're not mantaining this anymore
This was gnuxie's first project, it's bad, but that's a good thing.

No idea if this still works, Postmill 2.0 has happened since and I no longer browse any postmill instances.  

### What is new?
There are now new navigation classes for comments, submissions and paginators (a paginator being any page with articles e.g. /comments or a forum)

see the [changelog](./CHANGELOG.md) for details

### Hello world
```lisp
(defvar *cookie-jar* (make-instance 'drakma:cookie-jar))
(ppump:login "cl-postpump" "*****" *cookie-jar*)
(ppump:post-comment "https://community.postmill.xyz/50000" "hello world!!" *cookie-jar*)
```

### It is recommended that all users at least open the [**reference**](./docs/reference.md) and [**examples**](./docs/examples.md) of usage page


### Installation
This library is provided as an [asdf](https://common-lisp.net/project/asdf/) package, I recommend loading the package via [quicklisp](https://www.quicklisp.org/beta/) which will download and manage the dependencies for you. 


#### example installation 
This example uses [quicklisp](https://www.quicklisp.org/beta/)  to manage the project and its dependencies. 
Simply clone the repository into `~/quicklisp/local-projects/ `
```
cd ~/quicklisp/local-projects/
git clone https://gitlab.com/Gnuxie/cl-postpump.git
```
Then to load cl-postpump into your project use:
```lisp
(ql:quickload :cl-postpump)
```
And that's it, you're now all set!


### Help
Documentation for this project can be found [here](./docs/index.md) .


### License
This project is licensed under the GNU Affero General Public License V3 view [LICENSE.md](./LICENSE.md) for details


    cl-postpump
    Copyright (C) 2018  Gnuxie

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
