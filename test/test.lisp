(in-package :cl-user)
(defpackage :cl-postpump-test
  (:use :cl)
  (:export :cl-postpump))

(in-package :cl-postpump-test)
(setf *username* "cl_postpump_test")
(setf *password* "please do not change this")
(setf cl-postpump:*postmill-server* "https://raddle.me")

(defun test-login ()
  (setf *cookie-jar* (make-instance 'drakma:cookie-jar))
  (destructuring-bind (response-body status headers uri stream close-stream reason)
      (time (multiple-value-list (cl-postpump:login *username* *password* *cookie-jar*)))
    (equal (puri:render-uri (puri:parse-uri "https://raddle.me/") nil)
	   (puri:render-uri uri nil))))

(defun test-paginator-fetch ()
    (let ((a-paginator (make-instance 'cl-postpump:comment-paginator :feed "/comments")))
      (time (cl-postpump:paginator-fetch a-paginator))
      (setf *example-comment* (elt (cl-postpump:paginator-articles a-paginator) 0)))
    t)

(defun test-comment-vote ()
  (destructuring-bind (response-body status headers uri stream close-stream reason)
      (time (multiple-value-list (cl-postpump:vote "1" *example-comment* *cookie-jar*)))
    (if (equal (puri:render-uri (puri:parse-uri "https://raddle.me/") nil)
	       (puri:render-uri uri nil))
	(progn (cl-postpump:vote "0" *example-comment* *cookie-jar*)
	       t)
	nil)))

(defun test-submission-paginator-fetch ()
  (let ((a-paginator (make-instance 'cl-postpump:submission-paginator :feed "/")))
    (time (cl-postpump:paginator-fetch a-paginator)))
  t)

(defun test-comment-reply ()
  (let ((parsed-page (cl-postpump:get-request "https://raddle.me/f/TestGround/39956"))
	;; we should insert something random in here each time
	(comment-body "hello test"))
    (setf *new-comment* (time (cl-postpump:reply (cl-postpump:parse-to-comment (elt (lquery:$ parsed-page "article") 1)) comment-body *cookie-jar*))))
  (equal (cl-postpump:body *new-comment*) "<p>hello test</p>"))

(defun test-delete-comment ()
  (time (cl-postpump:article-delete *new-comment* *cookie-jar*))
  t)

(defun test-submission-reply ()
  (let ((parsed-page (cl-postpump:get-request "https://raddle.me/f/TestGround/30950/two-libertarian-reasons-conservatives-should-oppose-trump-s"))
	(comment-body (format nil
		              "# big header~%~
                               ## little header~%~
                               ~c* bullet point~%~
                               [link](https://community.postmill.xyz)" #\Space)))
    (let ((new-submission (cl-postpump:parse-to-submission (lquery:$1 parsed-page ".submission"))))
      (let ((new-comment-1 (time (cl-postpump:reply new-submission comment-body *cookie-jar*))))
	(cl-postpump:article-delete new-comment-1 *cookie-jar*)
	(equal (cl-postpump:body new-comment-1) "<h1>big header</h1><h2>little header</h2><ul><li>bullet point
<a href=\"https://community.postmill.xyz\" rel=\"nofollow\">link</a>
</li>
</ul>")))))

(defun test-parse-submission-body ()
  (let ((parsed-page (cl-postpump:get-request "https://raddle.me/f/freeAsInFreedom/37150/tipue-search-deleted-its-git-history"))
	(test-data "<p>It used to live at <a href=\"https://github.com/Tipue/Tipue-Search\" rel=\"nofollow\">https://github.com/Tipue/Tipue-Search</a></p><p>I recovered as many Git commits as I could, I uploaded it here: <a href=\"https://notabug.org/jorgesumle/Tipue-Search\" rel=\"nofollow\">https://notabug.org/jorgesumle/Tipue-Search</a>. The code it's still free software and can be freely downloaded from <a href=\"http://www.tipue.com/search/\" rel=\"nofollow\">their website</a>, sure, but how can I trust it, if I cannot see the changes introduced with each new version?</p><p>The original developer had a toxic relation with contributors. Usually they would get pull requests, but they ignore them completely without any feedback. Then, after 6 months or so, they included them in a new release. I was one of those contributors.</p><p>I use a modified version of the code. For that reason, I'd like to keep its history open and include improvements and fixes as long as they arrive. As the main developer cannot be trusted, I think the best option is to include features directly without sending them, so one option for me is to keep the development in the linked Git repository.</p>"))
    (let* ((article (lquery:$1 parsed-page ".submission"))
	   (submission (time (cl-postpump:parse-to-submission article))))
      (setf *example-submission* submission)
      (equal test-data (cl-postpump:body submission)))))

(defun test-article-get-replies ()
  (let ((replies (time (cl-postpump:replies *example-submission*))))
    (format t "replies :~%~S~%" replies)
    (if (not (equal 'ppump:missing replies))
	(not (= 0 (length replies)))
	nil)))

;;; this test won't age well
(defun test-paginator-regex ()
  (flet ((find-what (paginator end-comment-id)
	   (ppump:paginator-fetch paginator)
	   (let ((new-last-comment (ppump:id (cl-postpump:paginator-article paginator 0))))
	     (let ((whats (ppump:paginator-regex "^\\S*[wW]+[hH]+[aA]+[tT]+\\S*$" paginator end-comment-id)))
	       (setf end-comment-id new-last-comment)
	       whats))))
    
    (let ((end-comment-id 63837)
	  (new-comments (make-instance 'ppump:comment-paginator :feed "/user/cl_postpump_test/comments")))
      (let ((whats (time (find-what new-comments end-comment-id))))
	(equal (ppump:body (elt whats (- (length whats) 1))) "<p>what</p>")))))

(defun test-replies-find-if ()
  (let* ((test-submission (ppump:make-submission "/f/TestGround/30943/may-day-is-ours"))
	 (comment-result (time (ppump:replies-find-if #'(lambda (x) (= (ppump:id x)
								       64235))
						      test-submission))))
    (format t "id :~S~%" (ppump:id comment-result))
    (= (ppump:id comment-result) 64235)))

(defun test-comment-get-context ()
  "this test needs updating as soon as we test user pages for pagination and getting a given reply to a submission"
  (let* ((comment-paginator (make-instance 'ppump:comment-paginator :feed "/user/cl_postpump_test/comments"))
	 (a-comment (progn (ppump:paginator-fetch comment-paginator)
			   (ppump:articles-find-if #'(lambda (x)
						       (= (ppump:id x)
							  64235))
						   (ppump:paginator-articles comment-paginator))))
	 (the-submission (time (ppump:parent-submission a-comment))))

    (format t "the-submission : ~S~%" the-submission)
    (if (or (not the-submission) (equal 'ppump:missing the-submission))
	nil
	(let ((reply (ppump:replies-find-if #'(lambda (x)
						(= (ppump:id x)
						   (ppump:id a-comment)))
					    the-submission)))
	  (= 63838 (ppump:id (ppump:parent reply)))))))

(defun test-replies-remove-if-not ()
  (let* ((submission (ppump:make-submission "/f/TestGround/30943/may-day-is-ours"))
         (results (time (ppump:replies-remove-if-not #'(lambda (x) (equal "/user/cl_postpump_test"
									  (ppump:username x)))
						     submission))))
        (eql 3 (length results))))

(defun run-test (test)
  (format t "~%~%~S" test)
  (let ((result (if (funcall test)
		    'pass
		    'fail)))
    (format t "~%~S: ~S~%" test result)
    result))

(defun run-all ()
  (list (run-test #'test-login)
        (run-test #'test-paginator-fetch)
        (run-test #'test-comment-vote)
        (run-test #'test-submission-paginator-fetch)
	(run-test #'test-comment-reply)
	(run-test #'test-delete-comment)
	(run-test #'test-submission-reply)
	(run-test #'test-parse-submission-body)
	(run-test #'test-article-get-replies)
	(run-test #'test-paginator-regex)
	(run-test #'test-replies-find-if)
	(run-test #'test-comment-get-context)
	(run-test #'test-replies-remove-if-not)))

(let ((pass-and-fail (run-all)))
  (format t "~%~%[Summary cl-postpump-test ~S Passed and ~S Failed]~%" (count 'pass pass-and-fail) (count 'fail pass-and-fail)))
